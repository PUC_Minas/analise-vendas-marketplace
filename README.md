# Desenvolvimento e análise de dados para venads marketplace
## Trabalho integrado - PUC Pós-graduação de Gestão e Análise Estratégica de Dados

**Progresso:** Em desenvolvimento<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2021<br />

### Objetivo
Este trabalho tem como objetivo o desenvolvimento de uma solução de análise de dados para contexto de negócio, com foco em concretizar os conhecimentos obtidos durante a pós-graduação. O trabalho é dividido em três partes, sendo elas: ETL (extração, transformação e carga) da base de dados, construção de _dashboards_, análises avançadas sobre os dados.

### Observação

O artigo foi desenvolvido utilizando a plataforma  [OverLeaf](https://pt.overleaf.com/)

### Execução

    $ Clone o arquivo "trabalho-integrado.pbix"
    $ Abra o Power BI 
    $ Navegue no Power BI até a localização do arquivo
	$ Abra o arquivo


-- DROP SCHEMA dbo;

CREATE SCHEMA dbo;

-- vendas.dbo.bairro definition

-- Drop table

-- DROP TABLE vendas.dbo.bairro;

CREATE TABLE bairro (
	bairro_id bigint IDENTITY(1,1) NOT NULL,
	descricao nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_bairro PRIMARY KEY (bairro_id)
);


-- vendas.dbo.cidade definition

-- Drop table

-- DROP TABLE vendas.dbo.cidade;

CREATE TABLE cidade (
	cidade_id bigint IDENTITY(1,1) NOT NULL,
	descricao nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_cidade PRIMARY KEY (cidade_id)
);


-- vendas.dbo.cliente definition

-- Drop table

-- DROP TABLE vendas.dbo.cliente;

CREATE TABLE cliente (
	cliente_id bigint IDENTITY(1,1) NOT NULL,
	nome nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	documento nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	data_nascimento date NOT NULL,
	data_cadastro datetime NOT NULL,
	CONSTRAINT PK_cliente PRIMARY KEY (cliente_id)
);


-- vendas.dbo.estado definition

-- Drop table

-- DROP TABLE vendas.dbo.estado;

CREATE TABLE estado (
	estado_id bigint IDENTITY(1,1) NOT NULL,
	descricao nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_estado PRIMARY KEY (estado_id)
);


-- vendas.dbo.filial definition

-- Drop table

-- DROP TABLE vendas.dbo.filial;

CREATE TABLE filial (
	filial_id bigint IDENTITY(1,1) NOT NULL,
	nome nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_filial PRIMARY KEY (filial_id)
);


-- vendas.dbo.fornecedor definition

-- Drop table

-- DROP TABLE vendas.dbo.fornecedor;

CREATE TABLE fornecedor (
	fornecedor_id bigint IDENTITY(1,1) NOT NULL,
	nome nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_fornecedor PRIMARY KEY (fornecedor_id)
);


-- vendas.dbo.logradouro definition

-- Drop table

-- DROP TABLE vendas.dbo.logradouro;

CREATE TABLE logradouro (
	logradouro_id bigint IDENTITY(1,1) NOT NULL,
	descricao nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_logradouro PRIMARY KEY (logradouro_id)
);


-- vendas.dbo.loja definition

-- Drop table

-- DROP TABLE vendas.dbo.loja;

CREATE TABLE loja (
	loja_id bigint IDENTITY(1,1) NOT NULL,
	nome nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	parceiro bit NOT NULL,
	data_cadastro date NOT NULL,
	data_fim date NULL,
	CONSTRAINT PK_loja PRIMARY KEY (loja_id)
);


-- vendas.dbo.meio_pagamento definition

-- Drop table

-- DROP TABLE vendas.dbo.meio_pagamento;

CREATE TABLE meio_pagamento (
	meio_pagamento_id bigint IDENTITY(1,1) NOT NULL,
	descricao nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_meio_pagamento PRIMARY KEY (meio_pagamento_id)
);


-- vendas.dbo.pais definition

-- Drop table

-- DROP TABLE vendas.dbo.pais;

CREATE TABLE pais (
	pais_id bigint IDENTITY(1,1) NOT NULL,
	descricao nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_pais PRIMARY KEY (pais_id)
);


-- vendas.dbo.transportadora definition

-- Drop table

-- DROP TABLE vendas.dbo.transportadora;

CREATE TABLE transportadora (
	transportadora_id bigint IDENTITY(1,1) NOT NULL,
	nome nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK_transportadora PRIMARY KEY (transportadora_id)
);


-- vendas.dbo.cliente_endereco definition

-- Drop table

-- DROP TABLE vendas.dbo.cliente_endereco;

CREATE TABLE cliente_endereco (
	cliente_endereco_id bigint IDENTITY(1,1) NOT NULL,
	cliente_id bigint NOT NULL,
	numero int NOT NULL,
	cep int NOT NULL,
	complemento nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	logradouro_id bigint NOT NULL,
	cidade_id bigint NOT NULL,
	estado_id bigint NOT NULL,
	pais_id bigint NOT NULL,
	CONSTRAINT PK_cliente_endereco PRIMARY KEY (cliente_endereco_id),
	CONSTRAINT cliente_endereco_fk_cidade FOREIGN KEY (cidade_id) REFERENCES cidade(cidade_id),
	CONSTRAINT cliente_endereco_fk_cliente FOREIGN KEY (cliente_id) REFERENCES cliente(cliente_id),
	CONSTRAINT cliente_endereco_fk_estado FOREIGN KEY (estado_id) REFERENCES estado(estado_id),
	CONSTRAINT cliente_endereco_fk_logradouro FOREIGN KEY (logradouro_id) REFERENCES logradouro(logradouro_id),
	CONSTRAINT cliente_endereco_fk_pais FOREIGN KEY (pais_id) REFERENCES pais(pais_id)
);


-- vendas.dbo.pedido definition

-- Drop table

-- DROP TABLE vendas.dbo.pedido;

CREATE TABLE pedido (
	pedido_id uniqueidentifier NOT NULL,
	cliente_id bigint NOT NULL,
	cliente_endereco_id bigint NOT NULL,
	filial_id bigint NOT NULL,
	loja_id bigint NOT NULL,
	meio_pagamento_id bigint NOT NULL,
	data_hora_pedido datetime NOT NULL,
	CONSTRAINT PK_pedido PRIMARY KEY (pedido_id),
	CONSTRAINT pedido_fk_cliente FOREIGN KEY (cliente_id) REFERENCES cliente(cliente_id),
	CONSTRAINT pedido_fk_cliente_endereco FOREIGN KEY (cliente_endereco_id) REFERENCES cliente_endereco(cliente_endereco_id),
	CONSTRAINT pedido_fk_filial FOREIGN KEY (filial_id) REFERENCES filial(filial_id),
	CONSTRAINT pedido_fk_loja FOREIGN KEY (loja_id) REFERENCES loja(loja_id),
	CONSTRAINT pedido_fk_meio_pagamento FOREIGN KEY (meio_pagamento_id) REFERENCES meio_pagamento(meio_pagamento_id)
);


-- vendas.dbo.pedido_transportadora definition

-- Drop table

-- DROP TABLE vendas.dbo.pedido_transportadora;

CREATE TABLE pedido_transportadora (
	transportadora_id bigint NOT NULL,
	pedido_id uniqueidentifier NOT NULL,
	preco_frete decimal(15,2) NOT NULL,
	data_coleta datetime NOT NULL,
	data_entrega datetime NULL,
	CONSTRAINT PK_pedido_transportadora PRIMARY KEY (transportadora_id,pedido_id),
	CONSTRAINT pedido_transportadora_fk_pedido FOREIGN KEY (pedido_id) REFERENCES pedido(pedido_id),
	CONSTRAINT pedido_transportadora_fk_transportadora FOREIGN KEY (transportadora_id) REFERENCES transportadora(transportadora_id)
);


-- vendas.dbo.produto definition

-- Drop table

-- DROP TABLE vendas.dbo.produto;

CREATE TABLE produto (
	produto_id bigint IDENTITY(1,1) NOT NULL,
	nome nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	descricao nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	preco decimal(15,2) NOT NULL,
	fornecedor_id bigint NOT NULL,
	data_cadastro datetime NOT NULL,
	CONSTRAINT PK_produto PRIMARY KEY (produto_id),
	CONSTRAINT produto_fk_fornecedor FOREIGN KEY (fornecedor_id) REFERENCES fornecedor(fornecedor_id)
);


-- vendas.dbo.pedido_produto definition

-- Drop table

-- DROP TABLE vendas.dbo.pedido_produto;

CREATE TABLE pedido_produto (
	pedido_id uniqueidentifier NOT NULL,
	produto_id bigint NOT NULL,
	CONSTRAINT PK_pedido_produto PRIMARY KEY (pedido_id,produto_id),
	CONSTRAINT pedido_produto_fk_pedido FOREIGN KEY (pedido_id) REFERENCES pedido(pedido_id),
	CONSTRAINT pedido_produto_fk_produto FOREIGN KEY (produto_id) REFERENCES produto(produto_id)
);
